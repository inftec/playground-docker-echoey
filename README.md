# Overview

This is a simple echo server in a Docker container.

The server runs on port 8080 and returns whatever is sent to it, prefixed by a message and a timestamp.
