#!/bin/bash

# This script will start a server listening on PORT, returing MESSAGE and the current date, along with the input sent to the server.

pipe=/tmp/echo-pipe
log=/log/$DOCK_LOG_FILE_NAME
PORT=$DOCK_PORT
MESSAGE=$DOCK_MESSAGE

# Create a named pipe

rm -rf $pipe
mkfifo $pipe

# We'll use the named pipe to echo input sent to nc back
#   Input is processed by xargs which will make one echo call per Line (-L 1)
#   Echo will write the input after a Hello and the current date
#   tee sends the output from echo to the named pipe, along with an output to stdout
#   cat will catch up the output from the pipe and send it to nc
cat <$pipe|nc -k -l $PORT|xargs -L 1 echo $MESSAGE \(`date`\)|tee -a $log|tee $pipe 
